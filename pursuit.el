;; pursuit.el -- easy to use live feedback time report  -*- coding: utf-8 -*-

;; Copyright (C) 2023 Seppo Ronkainen.

;; This file is not part of GNU Emacs.

;; pursuit is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.

;; pursuit is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;; repository https://gitlab.com/sepron/pursuit

(set-face-attribute 'default nil :height 80)

(setq backup-inhibited t
      auto-save-default '()
      auto-save-list-file-prefix nil
      auto-save-interval 0
      inhibit-startup-buffer-menu t
      enable-local-variables :safe
      safe-local-variable-values '((encoding . utf-8))
      inhibit-startup-screen t
      create-lockfiles '()
      auto-revert-verbose '()
      auto-revert-remote-files t
      dired-auto-revert-buffer t)

(setq-default indent-tabs-mode '())

(defvar pursuit-emacs-debug (getenv "EMACSDEBUG")) ;; (setenv "EMACSDEBUG" "1")
(setq pursuit-emacs-debug t)
(message "{pursuit-emacs-debug %s}" pursuit-emacs-debug)
(if pursuit-emacs-debug
    (setq debug-on-error t))

(cond (load-file-name
       (defconst pursuit-home (file-name-directory load-file-name))
       (cd pursuit-home)
       (defconst effort-path (expand-file-name ".pursuit" (or (getenv "PURSUIT_HOME") pursuit-home)))
       (add-to-list 'load-path (expand-file-name "lisp" pursuit-home))
       ;; https://melpa.org/#/auto-highlight-symbol
       (cond ((locate-library "auto-highlight-symbol")
              (require 'auto-highlight-symbol)
              (global-auto-highlight-symbol-mode)
              (cond ((internal-lisp-face-p "ahs-plugin-defalt-face")
                     (set-face-background 'ahs-plugin-defalt-face "#303000")
                     (set-face-foreground 'ahs-plugin-defalt-face "#d0ff6a")))
              (cond ((internal-lisp-face-p "ahs-face")
                     (set-face-background 'ahs-face "#d0ff6a")
                     (set-face-foreground 'ahs-face "#000000")))
              (cond ((internal-lisp-face-p "ahs-definition-face")
                     (set-face-background 'ahs-definition-face "#d0ff6a")
                     (set-face-foreground 'ahs-definition-face "#000000")))))))

(defgroup pursuit-mode-faces '()
  "Faces for pursuit mode.")

(defface keyword-face-account
  '((((class grayscale) (background light)) :foreground "#000000" :slant italic)
    (((class grayscale) (background dark))  :foreground "#000000" :slant italic)
    (((class color) (min-colors 88) (background light)) :background "#400040" :foreground "#000000")
    (((class color) (min-colors 88) (background dark))  :background "#400040" :foreground "#ffffff")
    (((class color) (min-colors 16) (background light)) :foreground "#ff00ff")
    (((class color) (min-colors 16) (background dark))  :foreground "#0000ff")
    (((class color) (min-colors 8)) :foreground "black")
    (t :slant italic))
  "Font Lock mode face used to highlight effort lines."
  :group 'pursuit-mode-faces)

(defface keyword-face-charge
  '((((class color) (min-colors 88) (background dark))  :background "#0080ff" :foreground "#ffffff")
    (((class color) (min-colors 88) (background light)) :background "#0080ff" :foreground "#000000")
    (((class color) (min-colors 8)) :background "darkgreen" :foreground "white")
    (t :inverse-video t))
  "Font Lock mode face used to highlight charge lines."
  :group 'pursuit-mode-faces)

(defface keyword-face-estimate
  '((((class color) (min-colors 88) (background dark))  :background "#ffffff" :foreground "#000000")
    (((class color) (min-colors 88) (background light)) :background "#000000" :foreground "#000000")
    (((class color) (min-colors 8)) :background "white" :foreground "black")
    (t :inverse-video t))
  "Font Lock mode face used to highlight estimate lines."
  :group 'pursuit-mode-faces)

(defface keyword-face-comment
  '((((class color) (min-colors 88) (background dark))  :background "#100008" :foreground "#ff0066")
    (((class color) (min-colors 88) (background light)) :background "#100008" :foreground "#ff0066")
    (((class color) (min-colors 8)) :background "black" :foreground "magenta")
    (t :inverse-video t))
  "Font Lock mode face used to highlight commented lines."
  :group 'pursuit-mode-faces)

(defface keyword-face-option
  '((((class color) (min-colors 88) (background dark))  :background "#103010" :foreground "#80ff80")
    (((class color) (min-colors 88) (background light)) :background "#0080ff" :foreground "#000000")
    (((class color) (min-colors 8)) :background "darkgreen" :foreground "white")
    (t :inverse-video t))
  "Font Lock mode face used to highlight options."
  :group 'pursuit-mode-faces)

(setq
 pursuit-highlights
 '(("^account" . 'keyword-face-account)
   ("^charge" . 'keyword-face-charge)
   ("^estimate" . 'keyword-face-estimate)
   ("^option_hide_tool_bar$" . 'keyword-face-option)
   ("^option_popularized_cut_copy_paste_key_bindings$" . 'keyword-face-option)
   ("^option_daily_goal" . 'keyword-face-option)
   ("^option_estimate_ratio_threshold_red" . 'keyword-face-option)
   ("^option_estimate_ratio_threshold_orange" . 'keyword-face-option)
   ("^#.*" . 'keyword-face-comment)))

(define-derived-mode pursuit-mode prog-mode "pursuit"
  "major mode for pursuit (interactive time reporting)"
  (setq font-lock-defaults '(pursuit-highlights))
  (setq-local comment-start "#"))

(provide 'pursuit-mode)

(defvar pursuit-lines-cache '())

(defun pursuit-select-window-effort()
  (select-window (get-buffer-window (effort-buffer)))
  (pursuit-mode)
  (if (boundp 'auto-highlight-symbol-mode)
      (auto-highlight-symbol-mode)))

(defun pursuit-report-buffer-ensure()
  (let ((buf (get-buffer-create "*pursuit-report*")))
    (if (null (get-buffer-window buf))
        (display-buffer buf '()))
    buf))

(defun humanreadable-hours(hours)
  (let (h m hs hsl ms msl)
    (setq h (truncate hours)
          m (truncate (* (- hours h) 60.0))
          hs (and (< 0 h)
                  (format "%2d" h))
          hsl "h"
          ms (and (< 0 m)
                  (format "%2d" m))
          msl "m")
    (cond (hs
           (setq hsl "h")
           (put-text-property 0 (length hsl) 'face '(:background "#000000" :foreground "#004488") hsl)
           (setq hs (concat hs hsl))))
    (cond (ms
           (setq msl "m")
           (put-text-property 0 (length hsl) 'face '(:background "#000000" :foreground "#004488") msl)
           (setq ms (concat ms msl))))
    (format "%s%s"
            (or hs "   ")
            (or ms "   "))))

(defun accounts-description-from-account(account-name)
  (let (description it-a a)
    (setq it-a (pursuit-accounts))
    (while it-a
      (setq a (car it-a)
            it-a (cdr it-a))
      (if (null description)
          (if (string= (plist-get a 'name)
                       account-name)
              (setq description (plist-get a 'description)))))
    description))

(defun pursuit-report()
  (save-match-data
    (let (prb days it-da day day-timestamps day-ts durations sums day-wall-time accounts it-ac ac account-name charge-from-account estimates it-es es estimate-efforts charges it-ch charge s pre duration-effort fm to charge-factor account-belastning factored-hours it-du errors estimates-observed it-eo sum estimate fr rp burned-percent estimate-effort estimate-observed-effort hours-regarded daily-leasure day-hours-work sorted-sums it-ss ss week week-days week-hours weeks-table)
      (setq prb (pursuit-report-buffer-ensure)
            days (pursuit-days)
            accounts (pursuit-accounts)
            estimates (pursuit-estimates)
            estimate-efforts (mapcar (lambda(y)
                                       (plist-get y 'effort))
                                     estimates)
            charges (pursuit-charges)
            weeks-table (make-hash-table :test 'equal))
      (scan-for-options)
      (with-current-buffer prb
        (let ((inhibit-read-only t))
          (erase-buffer)
          ;; (text-scale-set 1)
          ;;(text-scale-decrease 1)
          (insert (format "# pursuit-report last updated %s\n"
                          (format-time-string "[%Y-%m-%d %a %H:%M:%S]")))
          (setq it-da (reverse days)
                estimates-observed '())
          (while it-da
            (setq day (car it-da)
                  it-da (cdr it-da)
                  day-timestamps (plist-get day 'timestamps)
                  day-ts (plist-get (car day-timestamps) 'timestamp)
                  week (format-time-string "%V" day-ts)
                  durations (pursuit-durations-from-timestamps day-timestamps)
                  sums '()
                  day-wall-time 0
                  s (format "%s" (plist-get day 'name)))
            (put-text-property 0 (length s) 'face '(:background "#0080ff" :foreground "#000000") s)
            (insert "\n"
                    (format-time-string "[%Y-%m-%d] %B " day-ts)
                    s
                    " (week "
                    week
                    ")\n")
            (setq it-du durations)
            (while it-du
              (setq du (car it-du)
                    it-du (cdr it-du)
                    duration-effort (plist-get du 'effort))
              (cond ((null (charge-p duration-effort))
                     (setq pre "  unrecognized charge-effort "
                           s (format "%s%s\n" pre duration-effort))
                     (put-text-property (length pre) (- (length s) 1) 'face '(:background "#880000") s)
                     (insert s)
                     (push duration-effort errors))))
            (cond ((null errors)
              (setq it-du durations)
              (while it-du
                (setq du (car it-du)
                      it-du (cdr it-du)
                      duration-effort (plist-get du 'effort)
                      fm (plist-get (plist-get du 'fm) 'timestamp)
                      to (plist-get (plist-get du 'to) 'timestamp)
                      hours (/ (float-time (time-subtract to fm)) 3600))
                (cond (hours
                       (setq day-wall-time (+ day-wall-time hours))
                       (insert (format "  %s to %s | %.3f %s %s\n"
                                       (format-time-string "%H:%M" fm)
                                       (format-time-string "%H:%M" to)
                                       hours
                                       (humanreadable-hours hours)
                                       duration-effort))
                       (cond ((member duration-effort estimate-efforts)
                              (let ((eoh (assoc duration-effort estimates-observed)))
                                (if eoh
                                    (setcdr eoh (append (list hours) (cdr eoh)))
                                  (push `(,duration-effort ,hours) estimates-observed)))))
                       (setq it-ch charges)
                       (while it-ch
                         (setq charge (car it-ch)
                               it-ch (cdr it-ch))
                         (let ((charge-effort (plist-get charge 'effort)))
                           (cond ((string= duration-effort charge-effort)
                                  (setq known '()
                                        it-ac accounts)
                                  (while it-ac
                                    (setq ac (car it-ac)
                                          it-ac (cdr it-ac)
                                          account-name (plist-get ac 'name)
                                          charge-from-account (assoc account-name (plist-get charge 'charges)))
                                    (cond (charge-from-account
                                           (setq known t
                                                 charge-factor (cadr charge-from-account)
                                                 account-belastning (assoc account-name sums)
                                                 factored-hours (* charge-factor hours))
                                           (if account-belastning
                                               (setcdr account-belastning
                                                       (append (list factored-hours)
                                                               (cdr account-belastning)))
                                             (push `(,account-name ,factored-hours)
                                                   sums)))))
                                  (cond ((null known)
                                         (setq s (format "warning unknown account %s"
                                                         (caar (plist-get charge 'charges))))
                                         (put-text-property 0 (length s) 'face '(:background "#ff4400") s)
                                         (insert (format " %s\n" s)))))))))))
              (setq hours-regarded 0
                    daily-leasure 0
                    sorted-sums (sort sums (lambda(l r)
                                             (string< (car l)
                                                      (car r))))
                    it-ss sorted-sums)
              (while it-ss
                (setq ss (car it-ss)
                      it-ss (cdr it-ss))
                (let ((h (apply '+ (cdr ss)))
                      (ad (accounts-description-from-account (car ss))))
                  (if ad
                      (put-text-property 0 (length ad) 'face '(:background "#000000" :foreground "#004488") ad))
                  (setq hours-regarded (+ hours-regarded h))
                  (if (string= (car ss) "leasure")
                      (setq daily-leasure (+ daily-leasure h)))
                  (insert (format "   %5.3f  %s   %s %s" h (humanreadable-hours h) (car ss) ad))
                  (if (< 1 (length (cdr ss)))
                      (let ((w (format " (+ %s)" (mapconcat (lambda(y) (format "%.3f" y)) (cdr ss) " "))))
                        (put-text-property 3 (- (length w) 1) 'face '(:background "#333333" :foreground "#000000") w)
                        (insert w))))
                (insert "\n"))
              (setq day-hours-work (- hours-regarded
                                      daily-leasure))
              (if (<= day-wall-time 0)
                  (let ((warning (format " warning this day did not seem to have any durations")))
                    (put-text-property 1 (length warning) 'face '(:background "#ff4400") warning)
                    (insert (format "%s\n" warning)))
                (insert "   +++++++++++++\n")
                (insert (format "      %10.3f   %s " hours-regarded "hours-regarded"))
                (cond ((string= (format "%10.3f" hours-regarded)
                                (format "%10.3f" day-wall-time))
                       (setq s "100%")
                       (put-text-property 0 (length s) 'face '(:background "#004400" :foreground "#000000") s)
                       (insert (format "%s" s))
                       (setq fr (/ day-hours-work
                                   daily-goal)
                             rp (* 100 fr)
                             s (format "%.3f%%" rp))
                       (if (< fr 1.0)
                           (put-text-property 0 (length s) 'face '(:background "#400000") s)
                         (put-text-property 0 (length s) 'face '(:background "#004000") s))
                       (insert (format " (%2.2f or %s = %s of daily goal %s)\n"
                                       day-hours-work
                                       (humanreadable-hours day-hours-work)
                                       s
                                       daily-goal)))
                      (t
                       (setq fr (/ hours-regarded
                                   day-wall-time)
                             rp (* 100 fr)
                             s (format "%f%%" rp))
                       (put-text-property 0 (length s) 'face '(:background "#880000") s)
                       (insert (format "%s\n      %10.3f   "
                                       s
                                       day-wall-time)
                               "from day-wall-time\n"))))))
            (setq w (gethash week weeks-table)
                  week-hours (plist-get w :hours)
                  week-days (+ (or (plist-get w :days)
                                   0)
                               1)
                  week-hours (+ (or week-hours
                                    0)
                                (or day-hours-work
                                    0))
                  w (plist-put w :days week-days)
                  w (plist-put w :hours week-hours))
            (puthash week w weeks-table))
          (goto-char (point-min))
          (forward-line 1)
          (cond (estimates-observed
                 (insert "\nestimates\n")
                 (setq it-es estimates)
                 (while it-es
                   (setq es (car it-es)
                         it-es (cdr it-es)
                         estimate-effort (plist-get es 'effort)
                         it-eo estimates-observed)
                   (while it-eo
                     (setq eo (car it-eo)
                           it-eo (cdr it-eo)
                           estimate-observed-effort (car eo))
                     (cond ((string= estimate-effort estimate-observed-effort)
                            (setq sum (apply '+ (cdr eo))
                                  estimate (plist-get es 'estimate)
                                  fr (/ sum
                                        estimate)
                                  rp (* 100 fr)
                                  burned-percent (format "%.3f%%" rp))
                            (cond ((<= estimate-ratio-threshold-red fr)
                                   (put-text-property 0 (length burned-percent) 'face '(:background "#ff0000" "#ffffff") burned-percent))
                                  ((<= estimate-ratio-threshold-orange fr)
                                   (put-text-property 0 (length burned-percent) 'face '(:background "#ff8000" "#ffff00") burned-percent)))
                            (insert (format "   %s %s = %s of estimated %g\n"
                                            (car eo)
                                            (humanreadable-hours sum)
                                            burned-percent estimate))))))
                 (insert "\n")))
          (insert "weeks\n")
          (maphash (lambda(y &rest other)
                     (let (w wd wh wg wp wo)
                       (cond ((setq w (car other))
                              (setq wd (plist-get w :days)
                                    wh (plist-get w :hours)
                                    wg (* wd daily-goal)
                                    wp (round (* 100
                                                 (/ wh
                                                    wg)))
                                    wo (- wh wg))
                              (cond ((and (< (/ -15.0 60.0) wo)
                                          (< wo (/ 15.0 60.0)))
                                     (setq wo "match reference"))
                                    ((< wo 0)
                                     (setq wo (format "undershoot %s" (humanreadable-hours (- 0 wo)))))
                                    (t
                                     (setq wo (format "overshoot  %s" (humanreadable-hours wo)))))
                              (insert (format "  [%s] {days %d} {hours %5s} %3d%% %s\n"
                                              y wd (/ (round wh 0.01) 100.0) wp wo))))))
                   weeks-table)
          (set-buffer-modified-p '())
          (read-only-mode t))))))

(defun pursuit-effort-changed(a b c)
  (cond ((string= effort-path (buffer-file-name (current-buffer)))
         (setq pursuit-lines-cache '())
         (pursuit-report))))

(add-hook 'after-change-functions 'pursuit-effort-changed)

(defvar daily-goal 8)
(defvar estimate-ratio-threshold-red 0.5)
(defvar estimate-ratio-threshold-orange 0.25)

(defun scan-for-options()
  (let (it-li li i f)
    (setq it-li (pursuit-lines))
    (while it-li
      (setq li (car it-li)
            it-li (cdr it-li))
      (cond ((null (string-match-p "^option_" li)))
            ((string-match "^option_popularized_cut_copy_paste_key_bindings" li)
             ;; https://en.wikipedia.org/wiki/Cut,_copy,_and_paste
             (global-set-key (kbd "C-z") 'undo)
             (global-set-key (kbd "C-x") 'kill-region)
             (global-set-key (kbd "C-c") 'kill-ring-save)
             (global-set-key (kbd "C-v") 'yank))
            ((string-match "^option_hide_tool_bar" li)
             (tool-bar-mode 0))
            ((string-match (rx line-start
                               "option_daily_goal"
                               (1+ blank)
                               (group (1+ (or "." digit))))
                           li)
             (setq i (match-string 1 li)
                   i (and i
                          (string-to-number i)))
             (if (and i
                      (< 0 daily-goal)
                      (< i 24))
                 (setq daily-goal i)))
            ((string-match (rx line-start
                               "option_estimate_ratio_threshold_red"
                               (1+ blank)
                               (group (1+ (or "." digit))))
                           li)
             (setq f (match-string 1 li)
                   f (and f
                          (string-to-number f)))
             (if (and f
                      (< 0 f)
                      (<= f 1.0))
                 (setq estimate-ratio-threshold-red f)))
            ((string-match (rx line-start
                               "option_estimate_ratio_threshold_orange"
                               (1+ blank)
                               (group (1+ (or "." digit))))
                           li)
             (setq f (match-string 1 li)
                   f (and f
                          (string-to-number f)))
             (if (and f
                      (< 0 f)
                      (<= f 1.0))
                 (setq estimate-ratio-threshold-orange f)))))))

(defun pursuit-startup()
  (set-frame-size (selected-frame) 200 50)
  (run-at-time 0.5
               '()
               (lambda()
                 (delete-other-windows)
                 (pursuit-report)
                 (pursuit-select-window-effort)
                 (split-window-vertically)
                 (balance-windows)
                 (scan-for-options)
                 (pursuit-calendar-configuration)
                 (pursuit-select-window-effort)
                 (goto-char (point-max))
                 (menu-bar-mode 0)
                 (scroll-bar-mode 0)
                 (require 'linum)
                 (global-linum-mode t)
                 (setq visible-bell t
                       show-trailing-whitespace t)
                 (pursuit-report))))

(add-hook 'emacs-startup-hook 'pursuit-startup)

(defun pursuit-revert()
  (pursuit-report))

(add-hook 'after-revert-hook 'pursuit-revert)

(require 'org)

(setq org-startup-indented t
      org-hide-leading-stars t
      org-log-done t
      org-time-stamp-rounding-minutes '(0 1)
      org-icalendar-use-UTC-date-time t
      org-support-shift-select 'always
      org-completion-use-ido t
      org-todo-keywords (quote ((sequence "TODO(t)" "NEXT(n)" "ONIT(o)" "|" "DONE(d)")
                                (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)" "PHONE" "MEETING")))
      diary-mail-days 3)
(org-clock-persistence-insinuate)

(require 'rx)

(defun effort-buffer()
  (or (get-buffer ".pursuit")
      (find-file effort-path)))

(defun line-content-p(line)
  (cond ((null line)
         '())
        ((string-match (rx line-start (0+ blank) "#")
                       line)
         '())
        (t
         line)))

(ert-deftest line-content-p-should-reflect-nil-and-empty-string()
  (should (eq (line-content-p '()) nil))
  (should (eq (line-content-p "") "")))

(ert-deftest line-content-p-should-pass-content()
  (should (string= (line-content-p "   and another thing") "   and another thing")))

(ert-deftest line-content-p-should-refuse-commented-lines()
  (should (null (line-content-p "  #  but not this line 3"))))

(defun pursuit-lines()
  (or pursuit-lines-cache
      (setq pursuit-lines-cache (delq '()
                                      (mapcar 'line-content-p
                                              (remove ""
                                                      (split-string (with-temp-buffer
                                                                      (insert-buffer (effort-buffer))
                                                                      (goto-char (point-min))
                                                                      (replace-regexp "^#.*$" "")
                                                                      (buffer-substring-no-properties (point-min)
                                                                                                      (point-max)))
                                                                    "\n")))))))

(defun pursuit-account-from-line(line)
  (cond ((null line)
         '())
        ((string-match (rx line-start
                           "account"
                           (1+ blank)
                           (group (1+ (not blank)))
                           blank (group (1+ any)))
                       line)
         (list 'what "account"
               'name (match-string 1 line)
               'description (match-string 2 line)))
        ((string-match (rx line-start
                           "account"
                           (1+ blank)
                           (group (1+ (not blank))))
                       line)
         (list 'what "account"
               'name (match-string 1 line)
               'description "<nodescription>"))))

(ert-deftest pursuit-account-from-line-should-refuse-nil-and-empty-string()
  (should (eq (pursuit-account-from-line '()) '()))
  (should (eq (pursuit-account-from-line "") '())))

(ert-deftest pursuit-account-from-line-should-refuse-accounts-without-name()
  (should (eq (pursuit-account-from-line "account") '()))
  (should (eq (pursuit-account-from-line "account       ") '())))

(ert-deftest pursuit-account-from-line-should-provide-default-nodescription()
  (should (equal (pursuit-account-from-line "account A37")
                 '(what "account" name "A37" description "<nodescription>"))))

(ert-deftest pursuit-account-from-line-should-parse-descriptions()
  (should (equal (pursuit-account-from-line "account BK398250-124 when I work on fixing my bicycle")
                 '(what "account" name "BK398250-124" description "when I work on fixing my bicycle"))))

(defun pursuit-accounts()
  (delq '() (mapcar 'pursuit-account-from-line (pursuit-lines))))

(defun pursuit-estimate-from-line(line)
  (and line
       (string-match (rx line-start "estimate" (1+ blank)) line)
       (let (parts effort estimate-text estimate-hours)
         (setq parts (cdr (split-string line " " 'omit-nulls))
               effort (car parts)
               estimate-text (cadr parts)
               estimate-hours (and estimate-text
                                   (string-to-number estimate-text)))
         (and estimate-hours
              (list 'what "estimate"
                    'effort effort
                    'estimate estimate-hours)))))

(defun pursuit-charge-from-line(line)
  (and line
       (string-match (rx line-start "charge" (1+ blank)) line)
       (let (parts effort assignments account weight charges)
         (setq parts (cdr (split-string line " " 'omit-nulls))
               effort (car parts)
               assignments (cdr parts))
         (while assignments
           (setq account (car assignments)
                 weight (cadr assignments))
           (push (list account (if weight
                                   (string-to-number weight)
                                 1))
                 charges)
           (setq assignments (cddr assignments)))
         (and charges
              (list 'what "charge"
                    'effort effort
                    'charges charges)))))

(ert-deftest pursuit-charge-from-line-should-refuse-nil-and-empty-string()
  (should (eq (pursuit-charge-from-line '()) '())))

(ert-deftest pursuit-charge-from-line-should-default-to-100-charge-rate()
  (should (equal (pursuit-charge-from-line "charge TASK-7894 fix-load-crash")
                 '(what "charge" effort "TASK-7894" charges (("fix-load-crash" 1))))))

(ert-deftest pursuit-charge-from-line-should-accept-multiple-charge-distribution-factors()
  (should (equal (pursuit-charge-from-line "charge release-work-20oct_rc1 A11 0.5 A22 0.4 A33 0.1")
                 '(what "charge" effort "release-work-20oct_rc1" charges (("A33" 0.1) ("A22" 0.4) ("A11" 0.5))))))

(defun pursuit-estimates()
  (delq '() (mapcar 'pursuit-estimate-from-line (pursuit-lines))))

(defun pursuit-charges()
  (delq '() (mapcar 'pursuit-charge-from-line (pursuit-lines))))

(defun pursuit-efforts()
  (delq '() (mapcar (lambda(y) (plist-get y 'effort)) (pursuit-charges))))

(defun charge-p(charge-effort)
  (member charge-effort
          (mapcar (lambda(y)
                    (plist-get y 'effort))
                  (pursuit-charges))))

(defun pursuit-time-parse(time-text)
  (and time-text
       (ignore-errors (date-to-time time-text))))

(ert-deftest pursuit-time-parse-should-refuse-nil-empty-string-and-garbage()
  (should (eq (pursuit-time-parse '()) nil))
  (should (eq (pursuit-time-parse "") '()))
  (should (eq (pursuit-time-parse "blah") '())))

(ert-deftest pursuit-time-parse-should-accept-valid-time-specification()
  (should (string= (format-time-string "%Y-%m-%d %H:%M:%S"
                                       (pursuit-time-parse "2020-09-15 11:30"))
                   "2020-09-15 11:30:00")))

(ert-deftest pursuit-time-parse-should-not-suffer-from-time-coordiante-constraints()
  (should (string= (format-time-string "%Y-%m-%d %H:%M:%S"
                                       (pursuit-time-parse "9999-12-31 23:59:59"))
                   "9999-12-31 23:59:59")))

(defun pursuit-timestamp-from-line(line)
  (cond ((null line)
         '())
        ((string-match (rx line-start (0+ any) (group "[" (1+ any) "]") blank (group (1+ (not blank))) blank (group (1+ any))) line)
         (let ((ts (pursuit-time-parse (match-string 1 line)))
               (effort (match-string 2 line))
               (comment (match-string 3 line)))
           (and ts
                effort
                (list 'what "timestamp"
                      'timestamp ts
                      'effort effort
                      'comment comment))))
        ((string-match (rx line-start (0+ any) (group "[" (1+ any) "]") blank (group (1+ any))) line)
         (let ((ts (pursuit-time-parse (match-string 1 line)))
               (effort (match-string 2 line)))
           (and ts
                effort
                (list 'what "timestamp"
                      'timestamp ts
                      'effort effort))))))

(ert-deftest pursuit-timestamp-from-line-should-refuse-nil-and-empty-strings()
  (should (eq (pursuit-timestamp-from-line '()) '()))
  (should (eq (pursuit-timestamp-from-line " ") '())))

(ert-deftest pursuit-timestamp-from-line-should-accept-valid-time-specification()
  (should (equal (pursuit-timestamp-from-line "[.] [2020-07-02 Thu 09:45] meeting this and that")
                 '(what "timestamp" timestamp (24317 36988) effort "meeting" comment "this and that"))))

(ert-deftest pursuit-timestamp-from-line-should-give-correct-time()
  (should (equal (format-time-string "%Y-%m-%d %H:%M:%S"
                                     (plist-get (pursuit-timestamp-from-line "[.] [3030-04-05 Thu 06:28] meeting this and that")
                                                'timestamp))
                 "3030-04-05 06:28:00")))

(defun pursuit-timestamps()
  (let ((buf (effort-buffer)))
    (if buf
        (sort (delq '() (mapcar 'pursuit-timestamp-from-line (pursuit-lines)))
              (lambda(l r)
                (time-less-p (plist-get l 'timestamp)
                             (plist-get r 'timestamp)))))))

(defun pursuit-durations-from-timestamps(timestamps)
  (let (it-ts ts durs prev)
    (setq it-ts timestamps)
    (while it-ts
      (setq ts (car it-ts)
            it-ts (cdr it-ts))
      (if (and prev
               (null (equal (plist-get prev 'timestamp)
                            (plist-get ts 'timestamp))))
          (push (list 'what "duration"
                      'fm prev
                      'to ts
                      'effort (or (plist-get prev 'effort)
                                  "<unknown effort>"))
                durs))
      (setq prev ts))
    (reverse durs)))

(defun pursuit-days()
  (let (days date today it-ts ts name)
    (setq it-ts (pursuit-timestamps))
    (while it-ts
      (setq ts (car it-ts)
            it-ts (cdr it-ts)
            name (format-time-string "%A %d" (plist-get ts 'timestamp)))
      (cond ((null (string= date name))
             (if today
                 (push (list 'what "day"
                             'name date
                             'timestamps (reverse today))
                       days))
             (setq today '()
                   date name)))
      (push ts today))
    (if today
        (push (list 'what "day"
                    'name date
                    'timestamps (reverse today))
              days))
    (reverse days)))

(defun pursuit-effort-entry()
  (interactive)
  (if (active-minibuffer-window)
      (exit-minibuffer))
  (select-window (get-buffer-window (find-file-noselect effort-path)))
  (let ((effort (ido-completing-read "effort: " (pursuit-efforts))))
    (goto-char (point-max))
    (insert (format " %s %s%s"
                    (format-time-string "[%Y-%m-%d %a %H:%M]")
                    effort
                    (format (if effort "\n" " "))))))

(global-set-key (kbd "<f1>") 'pursuit-effort-entry)

(set-face-background 'default "#000000")
(set-face-foreground 'default "#cceeff")
(set-face-background 'font-lock-comment-face "#000000")
(set-face-foreground 'font-lock-comment-face "#ff00af")
(set-face-background 'font-lock-string-face "#000000")
(set-face-foreground 'font-lock-string-face "#00ff00")
(set-face-background 'font-lock-type-face "#000000")
(set-face-foreground 'font-lock-type-face "#0080ff")
(set-face-background 'font-lock-constant-face "#004080")
(set-face-foreground 'font-lock-constant-face "#ffffff")
(set-face-background 'font-lock-keyword-face "#000000")
(set-face-foreground 'font-lock-keyword-face "#ffffff")
(set-face-background 'font-lock-function-name-face "#ffffff")
(set-face-foreground 'font-lock-function-name-face "#000000")
(set-face-background 'isearch "#ffff00")
(set-face-foreground 'isearch "#000000")
(set-face-background 'lazy-highlight "#dd0040")
(set-face-foreground 'lazy-highlight "#000000")
(set-face-background 'highlight "#aaff00")
(set-face-foreground 'highlight "#000000")

(set-face-background 'mode-line-inactive "#080000")
(set-face-foreground 'mode-line-inactive "#808080")

(set-face-background 'mode-line "#000018")
(set-face-foreground 'mode-line "#0080ff")
(set-face-attribute 'mode-line '() :box "#003000")

(ido-mode 1)

(defmacro pursuit-color-customization(face &rest body)
  (list 'if (not (internal-lisp-face-p face))
        (if pursuit-emacs-debug
            (message "{pursuit-color-customization unrecognized {face %s}} " face))
        (cons 'progn body)))

(cond (load-file-name
       (pursuit-color-customization "mode-line"
                                    (set-face-background 'mode-line "#000018")
                                    (set-face-foreground 'mode-line "#0080ff")
                                    (set-face-attribute  'mode-line '() :box "#003000"))
       (pursuit-color-customization "ido-first-match"
                                    (set-face-background 'ido-first-match "#ffff00")
                                    (set-face-foreground 'ido-first-match "#000000"))
       (pursuit-color-customization "ido-only-match"
                                    (set-face-background 'ido-only-match "#00ff00")
                                    (set-face-foreground 'ido-only-match "#000000"))
       (pursuit-color-customization "region"
                                    (set-face-background 'region "#ff00af")
                                    (set-face-foreground 'region "#000000"))))

(set-language-environment "UTF-8")
(set-language-environment-coding-systems "UTF-8")
(set-default-coding-systems 'utf-8)
(prefer-coding-system 'utf-8)
(require 'dired-x)

(show-paren-mode t)
(delete-selection-mode 1)
(put 'narrow-to-region 'disabled '())
(put 'narrow-to-page 'disabled '())
(put 'upcase-region 'disabled '())
(put 'downcase-region 'disabled '())
(put 'erase-buffer 'disabled '())
(put 'scroll-left 'disabled '())
(put 'dired-find-alternate-file 'disabled '())

(global-unset-key (kbd "C-x C-z"))
(global-unset-key (kbd "C-z"))

(defalias 'yes-or-no-p 'y-or-n-p)

(global-auto-revert-mode)
(add-hook 'dired-mode-hook 'auto-revert-mode)

(defface pursuit-calendar-face-default
  '((((class color) (min-colors 88) (background dark)) :background "#000000" :foreground "#baad99" :height 130)
    (((class color) (min-colors 8)) :background "black" :foreground "gray")
    (t :inverse-video t))
  "Font Lock mode face used on calendar."
  :group 'pursuit-calendar-faces)

(defun pursuit-calendar-configuration()
  (require 'calendar)
  (setq show-trailing-whitespace nil)
  (run-at-time 1
               '()
               (lambda()
                 (with-current-buffer (get-buffer "*Calendar*")
                   (calendar-mark-holidays)
                   (set-face-background 'holiday "#400000")
                   (set-face-foreground 'holiday "#ff0088"))
                 (copy-face font-lock-constant-face 'calendar-iso-week-face)
                 (set-face-background 'calendar-iso-week-face "#283328")
                 (set-face-foreground 'calendar-iso-week-face "#ffFFff")
                 (set-face-attribute 'calendar-iso-week-face nil
                                     :height 1.0)
                 (setq calendar-week-start-day 1
                       calendar-intermonth-header (propertize "wk"
                                                              'fond-lock-face
                                                              'calendar-iso-week-header-face)
                       calendar-intermonth-text '(propertize (format "%2d"
                                                                     (car (calendar-iso-from-absolute (calendar-absolute-from-gregorian (list month
                                                                                                                                              day
                                                                                                                                              year)))))
                                                             'font-lock-face 'calendar-iso-week-face))
                 (calendar)))
  (set (make-local-variable 'face-remapping-alist)
       '((default pursuit-calendar-face-default)))
  (redraw-display)
  (calendar)
  (run-at-time 1
               '()
               'other-window
               2))

(run-at-time 3
             '()
             'set-cursor-color
             "#37ccff")

