# pursuit

easy to use live feedback time report implemented in emacs-lisp

press key F1 to enter effort at current time coordinate (now), you can later edit and rearrange the time coordinates freely, the report redisplays accordingly


# convenience

create a directory named lisp as neighour to pursuit.sh and pursuit.el, then copy the auto highlight minor mode into a file "lisp/auto-highlight-symbol.el" from
https://melpa.org/#/auto-highlight-symbol


# screen capture example

![example screen capture](20201029201352utc_pursuit_example_screen_capture.jpg)


# howto

pursuit reads lines from a file (located as a directory neighbour to the `pursuit.el` script file) named `.pursuit` (previously .effort).

Have you noticed that not everyone is a [Emacs](https://www.gnu.org/software/emacs/tour/) enthusiast? While learning to accept :unamused: that not everyone has good taste I aspire to help, so pursuit supports options for comfort and convenience. Just type on any line in the `.pursuit` file, preferably at the top to not get in the way of time coordinates described further down. 


```
option_hide_tool_bar
```
will leave the tool bar visible to allow point-and-click operations `cut` `copy` `paste` `save` among others.



```
option_popularized_cut_copy_paste_key_bindings
```
will activate (upon startup of pursuit) popular key bindings (this will override the default Emacs Control-X-prefix)

| key sequence | operation |
| --- | --- |
| Ctrl-z | undo |
| Ctrl-x | cut |
| Ctrl-c | copy |
| Ctrl-v | paste |

however I strongly recommend you comment the option `#option_popularized_cut_copy_paste_key_bindings`, just get over it and learn the few standard Emacs key commands and when needed fall back on clicking the icons when unsure. Note that Meta may be the Alt key on your keyboard mkay.

| key sequence | operation |
| --- | --- |
| Ctrl-_ | "undo" |
| Ctrl-w | "cut" (kill-region) |
| Meta-w |  "copy" (kill-ring-save) |
| Ctrl-y | "paste" (yank) |



Let's familiarize ourselves with the `.pursuit` file

You may declare your accounts on any line, start with the reserved word `account` followed by some account name (such as A3985723985) and then a description (for instance "integration support")

```
account A3985723985 "integration support"
```

pursuit allows us to declare charges on any line, start with the reserved word `charge` followed by some effort name (such as TASK-7777) and then a list of account names with charge factors

```
charge TASK-7777 account4444 0.2 account9999 0.4 account4411 0.4
```


enough talk :grimacing: .. let me try this out in practice already .. for starters place the following text into `.pursuit`

```
# comments start with number sign aka hash #

account leasure "leasure time"
account A3985723985 "integration support"
account DEV83752 "development of software for customer XYZ"
account GEN168269 "general account for integration toward product WXY"
account stud387562 "studies and education"

charge TASK-1111 DEV83752
charge TASK-create-release GEN168269 0.2 A3985723985 0.8
charge lunch_break leasure
charge bike_home leasure

 [2020-09-17 Thu 07:52] TASK-44
 [2020-09-17 Thu 11:46] lunch_break
```

in the right buffer the report lists an error message saying that the text TASK-44 was not recognized and pursuit can not deduce how to charge the accounts.

![unrecognized](report_error_unrecognized_charge_effort_task_44.png "unrecognized")



Edit the time coordinate line `[2020-09-17 Thu 07:52] TASK-44` to instead read `[2020-09-17 Thu 07:52] TASK-1111`

now the report should state that 3.9 hours was charged on account DEV83752 via the charge TASK-1111

![3.9 hours](report_3_9_hours_on_account_dev83752.png "3.9 hours")




As the weeks progresses you work on diffent tasks, each time you change task hit the keyboard key F1 to be prompted for a effort to switch to.

a time coordinate is automatically inserted at the bottom of the `.pursuit` file. You can edit the time coordinates and the report will automatically update accordingly



Some tasks may require you to charge multiple accounts via some factors, as an example the `charge TASK-create-release GEN168269 0.2 A3985723985 0.8` line places 20% of the hours on account GEN168269 and 80% on account A3985723985. Omitted charge factor/weight is persumed to be 1.0 which in turn means 100% of the time in the duration in question.




now append to the `.pursuit` file

```
 [2020-09-18 Fri 07:52] TASK-create-release
 [2020-09-18 Fri 16:30] bike_home
```

and see the report distributing hours over the two charged accounts

![hours distributed over weighted accounts](hours_distributed_over_weighted_accounts.png)


if the charge weights are not balanced the report will indicate this with red text for how much time was regarded.

![hours regarded not exactly balanced](hours_regarded_not_exactly_balanced.png)


```
charge TASK-create-release GEN168269 0.2 A3985723985 0.83
```
either the factor 0.2 or 0.83 require adjustments to even the sum to a 1.0 (100%)

Alter the weights at the corresponding charge line until the report once again indicates a green 100% for regarded hours.


estimates for listed tasks will be presented to indicate match between estimated time and time spent


now also presents weekly goal matches and under/over-shoot


You should also be aware of the fact that pursuit has been designed to automagically update the report after external edits to the .effort file (unless the local buffer has been modified .. then the auto-revert mechanism cowardly refuses to overwrite your local edits)


